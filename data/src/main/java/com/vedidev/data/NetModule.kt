package com.vedidev.data

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.vedidev.data.api.ApiManager
import com.vedidev.data.entities.response.ResponseDeserializer
import com.vedidev.data.entities.response.container.ResponseData
import com.vedidev.domain.common.Constants
import com.vedidev.domain.utils.DateTimeUtils
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class NetModule(internal var baseUrl: String) {

    @Provides
    @Singleton
    internal fun provideGson(responseDeserializer: ResponseDeserializer): Gson {
        return GsonBuilder()
                .setDateFormat(DateTimeUtils.ISO_TIME_FORMAT)
                .registerTypeAdapter(ResponseData::class.java, responseDeserializer)
                .create()
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient().newBuilder()
        builder.readTimeout(Constants.DEFAULT_SERVER_READ_TIMEOUT, TimeUnit.SECONDS)
        builder.connectTimeout(Constants.DEFAULT_SERVER_CONNECT_TIMEOUT, TimeUnit.SECONDS)
        builder.addInterceptor { chain ->
            val request = chain.request()
            Log.d(Constants.APP_TAG, request.url().toString())
            val newRequest = chain.request().newBuilder()
            newRequest.url(chain.request().url().toString() + "&apiKey=${Constants.API_KEY}")
            chain.proceed(newRequest.build())
        }
        builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        return builder.build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    internal fun provideApiManager(retrofit: Retrofit): ApiManager {
        return ApiManager(retrofit)
    }
}
