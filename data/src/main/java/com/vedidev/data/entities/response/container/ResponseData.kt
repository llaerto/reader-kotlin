package com.vedidev.data.entities.response.container

class ResponseData<T>(@com.google.gson.annotations.SerializedName(value = "result") val data: T?) {

    val isSuccessful: Boolean
        get() = data != null
}
