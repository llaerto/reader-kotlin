package com.vedidev.data.entities.beans

import com.google.gson.annotations.SerializedName
import ninja.sakib.pultusorm.annotations.Ignore
import ninja.sakib.pultusorm.annotations.PrimaryKey

class Article {
    @SerializedName("author") var author: String? = null
    @SerializedName("title") var title: String? = null
    @SerializedName("description") var description: String? = null
    @SerializedName("url") @PrimaryKey var url: String? = null
    @SerializedName("urlToImage") var urlToImage: String? = null
    @SerializedName("publishedAt") var publishedAt: String? = null
}