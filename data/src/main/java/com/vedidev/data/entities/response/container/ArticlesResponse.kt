package com.vedidev.data.entities.response.container

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.vedidev.data.entities.beans.Article

class ArticlesResponse {
    @SerializedName("status") var status: String? = null
    @SerializedName("source") var source: String? = null
    @SerializedName("sortBy") var sortBy: String? = null
    @SerializedName("articles") var articles: ArrayList<Article>? = null

    override fun toString(): String {
        return Gson().toJson(this)
    }
}
