package com.vedidev.data.entities.response

import com.google.gson.*
import com.vedidev.data.entities.response.container.ResponseData

import java.lang.reflect.Type
import javax.inject.Inject

class ResponseDeserializer @Inject
constructor() : JsonDeserializer<ResponseData<*>> {

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ResponseData<*> {
        return Gson().fromJson<ResponseData<*>>(json.asJsonObject, typeOfT)
    }
}
