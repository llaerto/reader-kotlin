package com.vedidev.data.entities.response.container

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.vedidev.data.entities.beans.Source

/**
 * Created by llaerto on 05-Jun-17.
 */

class SourcesResponse {
    @SerializedName("status") var status: String? = null
    @SerializedName("sources") var sources: ArrayList<Source>? = null

    override fun toString(): String {
        return Gson().toJson(this)
    }
}
