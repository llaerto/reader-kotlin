package com.vedidev.data

import com.vedidev.data.db.DBManager
import dagger.Module
import dagger.Provides
import ninja.sakib.pultusorm.core.PultusORM
import javax.inject.Singleton

@Module
class DataModule(val path: String) {

//    @Provides @Singleton internal fun providePultusORM() : PultusORM {
//        return PultusORM("newsapi.db", path)
//    }

    @Provides @Singleton internal fun provideDBManager() : DBManager {
        return DBManager(PultusORM("newsapi.db", path))
    }
}
