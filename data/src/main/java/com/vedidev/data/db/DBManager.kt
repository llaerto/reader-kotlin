package com.vedidev.data.db

import com.vedidev.data.entities.beans.Article
import io.reactivex.Observable
import ninja.sakib.pultusorm.callbacks.Callback
import ninja.sakib.pultusorm.core.PultusORM
import ninja.sakib.pultusorm.core.PultusORMCondition
import ninja.sakib.pultusorm.core.PultusORMQuery
import ninja.sakib.pultusorm.exceptions.PultusORMException
import javax.inject.Inject

class DBManager @Inject constructor(pultus: PultusORM) {
    private var pultusOrm: PultusORM = pultus

    fun getArticles(): Observable<ArrayList<Article>> {
        return Observable.create({ e ->
            e.onNext(pultusOrm.find(Article()) as ArrayList<Article>)
        })
    }

    fun putArticle(article: Article): Observable<Article> {
        return Observable.create({ e ->
            pultusOrm.save(article, object : Callback {
                override fun onFailure(type: PultusORMQuery.Type, exception: PultusORMException) {
                    e.onError(exception)
                }

                override fun onSuccess(type: PultusORMQuery.Type) {
                    e.onNext(article)
                }
            })
        })

    }

    fun removeArticle(article: Article): Observable<Article> {
        return Observable.create({ e ->
            val condition = PultusORMCondition.Builder().eq("url", article.url!!).build()
            pultusOrm.delete(Article(), condition, object : Callback {
                override fun onFailure(type: PultusORMQuery.Type, exception: PultusORMException) {
                    e.onError(exception)
                }

                override fun onSuccess(type: PultusORMQuery.Type) {
                    e.onNext(article)
                }
            })
        })
    }

    fun checkArticle(article: Article): Observable<Boolean> {
        return Observable.create({ e ->
            //            val condition = PultusORMCondition.Builder().eq("url", article.url!!).build()
            val articles = pultusOrm.find(Article()) as ArrayList<Article>
            article.description = article.description?.replace("\'", "\'\'", false)
            article.author = article.description?.replace("\'", "\'\'", false)
            article.title = article.description?.replace("\'", "\'\'", false)
            article.publishedAt = article.description?.replace("\'", "\'\'", false)

            var articleNew: Article? = null
            (0..(articles.size - 1)).map {
                if (article.url == articles[it].url) {
                    articleNew = article
                }
            }
            e.onNext(articleNew != null)
        })
    }
}