package com.vedidev.data.api.retrofit

import com.vedidev.data.entities.response.container.ArticlesResponse
import com.vedidev.data.entities.response.container.SourcesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiInterface {
    @GET("sources")
    fun getSources(@Query("language") language: String, @Query("category") category: String)
            : Observable<SourcesResponse>

    @GET("articles") fun getArticles(@Query("source") source: String)
            : Observable<ArticlesResponse>
}
