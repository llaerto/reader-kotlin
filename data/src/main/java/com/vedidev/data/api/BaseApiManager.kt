package com.vedidev.data.api

public abstract class BaseApiManager<T>(retrofit: retrofit2.Retrofit, clazz: Class<T>) {

    var apiInterface: T = retrofit.create(clazz)

}
