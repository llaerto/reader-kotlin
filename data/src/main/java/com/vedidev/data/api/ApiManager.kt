package com.vedidev.data.api

import com.vedidev.data.api.retrofit.ApiInterface
import com.vedidev.data.entities.response.container.ArticlesResponse
import com.vedidev.data.entities.response.container.SourcesResponse
import io.reactivex.Observable
import retrofit2.Retrofit

class ApiManager constructor(retrofit: Retrofit) : BaseApiManager<ApiInterface>(retrofit, ApiInterface::class.java) {
    fun getSources(category: String) : Observable<SourcesResponse> {
        return apiInterface.getSources("en", category)
    }

    fun getArticles(source: String) : Observable<ArticlesResponse> {
        return apiInterface.getArticles(source)
    }
}
