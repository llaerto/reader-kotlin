package com.vedidev.meduzareader.events

import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject

class RxBus {

    private val bus = PublishSubject.create<Any>();

    fun send(o: Any) {
        bus.onNext(o);
    }

    fun <T> register(eventClass: Class<T>, onNext: Consumer<T>): Disposable {
        return bus
                .filter { event -> event.javaClass == eventClass }
                .map<T> { obj -> obj as T }
                .subscribe(onNext);
    }
}
