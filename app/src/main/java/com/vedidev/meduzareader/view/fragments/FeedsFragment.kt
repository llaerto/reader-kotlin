package com.vedidev.meduzareader.view.fragments

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.bindView
import com.vedidev.data.entities.beans.Article
import com.vedidev.meduzareader.R
import com.vedidev.meduzareader.interfaces.presenter.FeedsPresenterActions
import com.vedidev.meduzareader.interfaces.view.FeedsViewActions
import com.vedidev.meduzareader.presenter.FeedsPresenter
import com.vedidev.meduzareader.view.adapters.ArticlesAdapter

class FeedsFragment : BaseFragment<FeedsPresenterActions.ViewActions>(),
        FeedsViewActions, ArticlesAdapter.CardClickListener {
    companion object {
        private val key = "key"

        fun newInstance(type: String): FeedsFragment {
            val fragment = FeedsFragment()
            val args = Bundle()
            args.putString(key, type)
            fragment.arguments = args
            return fragment
        }
    }

    var listener: FragmentEventsListener? = null
    val recyclerView: RecyclerView? by bindView(R.id.recycler_view)
    var adapter: ArticlesAdapter? = null

    override val layoutResId: Int = R.layout.fragment_feeds
    override val tagName: String = FeedsFragment::class.java.simpleName
    override fun createPresenter(): FeedsPresenterActions.ViewActions = FeedsPresenter(this)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FragmentEventsListener) {
            listener = context
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view?.setBackgroundColor(Color.WHITE)
        presenter?.onViewCreated()
    }

    override fun getType(): String? {
        if (arguments != null && arguments.containsKey(key)) {
            return arguments.getString(key)
        } else return null
    }

    override fun showFeeds(feeds: List<Article>) {
        adapter = ArticlesAdapter(feeds)
        adapter?.setCardClickListener(this)
        recyclerView?.adapter = adapter
        recyclerView?.layoutManager = LinearLayoutManager(context)
    }

    override fun refreshAdapter() {
        adapter?.notifyDataSetChanged()
    }

    override fun onCardClick(article: Article) {
        presenter?.onCardClick(article)
    }

    override fun launchFeedActivity(feedIntent: Intent) {
        listener?.launchFeedActivity(feedIntent)
    }

    interface FragmentEventsListener {
        fun launchFeedActivity(feedIntent: Intent)
    }
}
