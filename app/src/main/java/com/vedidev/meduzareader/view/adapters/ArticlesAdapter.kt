package com.vedidev.meduzareader.view.adapters

import android.graphics.drawable.Drawable
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.bindView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.vedidev.data.entities.beans.Article
import com.vedidev.meduzareader.R

class ArticlesAdapter(var articles: List<Article>) : RecyclerView.Adapter<ArticlesAdapter.ArticlesListHolder>() {

    private var listener: CardClickListener? = null

    override fun getItemCount(): Int = articles.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ArticlesListHolder =
            ArticlesListHolder(LayoutInflater.from(parent?.context).inflate(R.layout.card_article, parent, false))

    override fun onBindViewHolder(holder: ArticlesListHolder?, position: Int) {
        holder?.title?.text = articles[position].title
        holder?.date?.text = articles[position].publishedAt
        holder?.imageLayout?.visibility = View.VISIBLE
        Glide
                .with(holder?.image?.context)
                .load(articles[position].urlToImage)
                .listener(object : RequestListener<Drawable> {
                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        holder?.progressBar?.visibility = View.GONE
                        return false
                    }

                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        return false
                    }
                }).into(holder?.image)
        holder?.card?.setOnClickListener { listener?.onCardClick(articles[position]) }
    }

    class ArticlesListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView? by bindView(R.id.article_title)
        val image: ImageView? by bindView(R.id.article_image)
        val date: TextView by bindView(R.id.article_date)
        val progressBar: ProgressBar by bindView(R.id.article_progress_bar)
        val imageLayout: ConstraintLayout by bindView(R.id.layout_image)
        val card: CardView by bindView(R.id.card_view)
    }

    fun setCardClickListener(cardClickListener: CardClickListener) {
        this.listener = cardClickListener
    }

    interface CardClickListener {
        fun onCardClick(article: Article)
    }
}
