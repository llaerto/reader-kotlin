package com.vedidev.meduzareader.view.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import butterknife.bindView
import com.vedidev.domain.common.AppError
import com.vedidev.meduzareader.R
import com.vedidev.meduzareader.interfaces.presenter.DrawerPresenterActions
import com.vedidev.meduzareader.interfaces.view.DrawerViewActions
import com.vedidev.meduzareader.presenter.DrawerPresenter
import com.vedidev.meduzareader.view.fragments.CategoriesFragment
import com.vedidev.meduzareader.view.fragments.FeedsFragment

class DrawerActivity
    : BaseViewActivity<DrawerPresenterActions.ViewActions>(),
        DrawerViewActions, CategoriesFragment.FragmentEventsListener, FeedsFragment.FragmentEventsListener {

    val drawer: DrawerLayout? by bindView(R.id.drawer)
    val navigationView: NavigationView? by bindView(R.id.nav_view)
    var drawerToggle: ActionBarDrawerToggle? = null

    override val tagName: String = DrawerActivity::class.java.simpleName
    override val layoutResId: Int = R.layout.activity_main_drawer
    override fun createPresenter(): DrawerPresenterActions.ViewActions = DrawerPresenter(this)
    override fun showError(error: AppError?) = shortSnack(error!!.message)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter?.onViewCreated()
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        drawerToggle = ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)
        drawer?.addDrawerListener(drawerToggle!!)
        navigationView?.inflateHeaderView(R.layout.layout_header)
        navigationView?.setNavigationItemSelectedListener { item ->
            presenter?.onItemSelect(item.itemId)
            true
        }
        drawerToggle?.syncState()
    }

    override fun showBookmarksFragment(feedsFragment: FeedsFragment) {
        clearBackStack()
        supportFragmentManager
                ?.beginTransaction()
                ?.add(R.id.fragment_container, feedsFragment, feedsFragment.tagName)
                ?.commit()
        drawer?.closeDrawers()
    }

    override fun showFeedsFragment(feedsFragment: FeedsFragment) {
        supportFragmentManager
                ?.beginTransaction()
                ?.add(R.id.fragment_container, feedsFragment, feedsFragment.tagName)
                ?.addToBackStack(feedsFragment.tagName)
                ?.commit()
        drawer?.closeDrawers()
    }

    override fun showCategoriesFragment(categoriesFragment: CategoriesFragment) {
        clearBackStack()
        supportFragmentManager
                ?.beginTransaction()
                ?.replace(R.id.fragment_container, categoriesFragment, categoriesFragment.tagName)
                ?.commit()
        drawer?.closeDrawers()
    }

    override fun launchFeedActivity(feedIntent: Intent) {
        startActivity(feedIntent)
    }

    private fun clearBackStack() {
        (0..(supportFragmentManager.backStackEntryCount)).map { supportFragmentManager.popBackStack() }
    }
}