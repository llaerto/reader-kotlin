package com.vedidev.meduzareader.view.fragments

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import butterknife.bindView
import com.vedidev.domain.common.AppError
import com.vedidev.domain.common.StateMaintainer
import com.vedidev.meduzareader.App
import com.vedidev.meduzareader.R
import com.vedidev.meduzareader.di.AppComponent
import com.vedidev.meduzareader.interfaces.BaseFragmentEventListener
import com.vedidev.meduzareader.interfaces.presenter.BasePresenterActions
import com.vedidev.meduzareader.interfaces.view.BaseViewActions

abstract class BaseFragment<T : BasePresenterActions.ViewActions> : Fragment(), BaseViewActions {

    internal val progressView: View? by bindView(R.id.progress_view)
    internal val emptyView: View? by bindView(R.id.empty_view)

    private var stateMaintainer: StateMaintainer? = StateMaintainer.instance
    private var fadeInAnimation: Animation? = null
    private var fadeOutAnimation: Animation? = null
    var presenter: T? = null

    protected var progressDialog: ProgressDialog? = null
    protected var baseFragmentEventListener: BaseFragmentEventListener? = null
    override var isUiLocked: Boolean = false

    /* Superclass methods */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = isRetain
        fadeOutAnimation = AnimationUtils.loadAnimation(context, R.anim.fast_fade_out)
        fadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fast_fade_in)
        restore()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseFragmentEventListener) {
            baseFragmentEventListener = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(layoutResId, container, false)
        return rootView
    }

    override fun onStart() {
        super.onStart()
        presenter?.onStart()
    }

    override fun onResume() {
        super.onResume()
        presenter?.onResume()
    }

    override fun onPause() {
        super.onPause()
        presenter?.onPause()
    }

    override fun onStop() {
        super.onStop()
        presenter?.onStop()
        hideKeyboard()
    }

    override fun onDestroyView() {
        presenter?.onViewDestroyed()
        stateMaintainer?.delete(tagName)
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter?.onDestroy()
        presenter = null
        super.onDestroy()
    }

    /* Abstract methods */

    protected abstract val layoutResId: Int

    protected abstract fun createPresenter(): T

    abstract val tagName: String

    // View actions

    override val viewContext: Context?
        get() = context

    override fun injector(): AppComponent? {
        return (activity.application as App).appComponent
    }

    override fun showProgressBar() {
        progressDialog?.show()
    }

    override fun showProgressView() {
        progressView?.visibility = View.VISIBLE
        emptyView?.visibility = View.GONE
    }

    override fun hideProgressView() {
        progressView?.visibility = View.GONE
    }

    override fun showEmptyView() {
        emptyView?.visibility = View.VISIBLE
    }

    override fun lockUi() {
        isUiLocked = true
    }

    override fun unlockUi() {
        isUiLocked = false
        hideProgress()
    }

    override fun hideKeyboard() {
        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0)
    }

    override fun showError(error: AppError?) {
        baseFragmentEventListener?.showError(error)
    }

    /* Inner methods */

    protected fun fadeAction(view: View, start: Boolean) {
        view.visibility = if (start) View.VISIBLE else View.GONE
        view.startAnimation(if (start) fadeInAnimation else fadeOutAnimation)
    }

    protected fun restore() {
        if (isRestoreAble) {
            try {
                if (stateMaintainer!!.firstTimeIn(activity.supportFragmentManager, tagName)) {
                    initialize()
                } else {
                    reinitialize()
                }
            } catch (e: Fragment.InstantiationException) {
                throw RuntimeException(e)
            } catch (e: IllegalAccessException) {
                throw RuntimeException(e)
            }

        } else {
            try {
                initialize()
            } catch (exception: Exception) {
                throw RuntimeException(exception)
            }
        }
    }

    protected open val isRestoreAble: Boolean = true

    protected val isRetain: Boolean = true

    protected fun resetView() {
        presenter?.setView(this)
    }

    @Throws(Fragment.InstantiationException::class, IllegalAccessException::class)
    private fun initialize() {
        presenter = createPresenter()
        if (isRestoreAble) {
            stateMaintainer!!.put(tagName, presenter)
        }
        initProgressDialog()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(context)
        progressDialog?.setTitle(progressTitle!!)
        progressDialog?.setMessage(progressMessage!!)
    }

    private fun hideProgress() {
        if (progressDialog != null) {
            progressDialog!!.dismiss()
        }
        if (progressView != null) {
            progressView!!.visibility = View.GONE
        }
    }

    @Throws(Fragment.InstantiationException::class, IllegalAccessException::class)
    private fun reinitialize() {
        presenter = stateMaintainer?.get<T>(tagName)
        if (presenter == null) {
            initialize()
        } else {
            resetView()
        }
    }

    protected val progressTitle: String?
        get() = getString(R.string.progress_title)

    protected val progressMessage: String?
        get() = getString(R.string.progress_message)

}
