package com.vedidev.meduzareader.view.fragments

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.bindView
import com.vedidev.data.entities.beans.Source
import com.vedidev.meduzareader.R
import com.vedidev.meduzareader.interfaces.presenter.CategoriesPresenterActions
import com.vedidev.meduzareader.interfaces.view.CategoriesViewActions
import com.vedidev.meduzareader.presenter.CategoriesPresenter
import com.vedidev.meduzareader.view.adapters.SourcesAdapter

class CategoriesFragment : BaseFragment<CategoriesPresenterActions.ViewActions>(), CategoriesViewActions,
        SourcesAdapter.CardClickListener {

    companion object {
        private val key = "key"

        fun newInstance(type: String): CategoriesFragment {
            val fragment: CategoriesFragment = CategoriesFragment()
            val args = Bundle()
            args.putString(key, type)
            fragment.arguments = args
            return fragment
        }
    }

    val recyclerView: RecyclerView by bindView(R.id.recycler_view)
    var adapter: SourcesAdapter? = null
    var listener: FragmentEventsListener? = null

    override val layoutResId: Int = R.layout.fragment_categories
    override val tagName: String = CategoriesFragment::class.java.simpleName
    override fun createPresenter(): CategoriesPresenterActions.ViewActions = CategoriesPresenter(this)
    override val isRestoreAble: Boolean = false

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FragmentEventsListener) {
            listener = context
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter?.onViewCreated()
    }

    override fun getType(): String? {
        if (arguments != null && arguments.containsKey(key)) {
            return arguments.getString(key)
        } else return null
    }

    override fun showItems(sources: ArrayList<Source>) {
        adapter = SourcesAdapter(sources)
        adapter?.setCardClickListener(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
    }

    override fun refreshAdapter() {
        adapter?.notifyDataSetChanged()
    }

    override fun showFeedsFragment(feedsFragment: FeedsFragment) {
        listener?.showFeedsFragment(feedsFragment)
    }

    override fun onCardClick(name: String) {
        presenter?.onCardClick(name)
    }

    interface FragmentEventsListener {
        fun showFeedsFragment(feedsFragment: FeedsFragment)
    }
}
