package com.vedidev.meduzareader.view.activities

import android.app.ActivityManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.Toast

import com.vedidev.meduzareader.R
import com.vedidev.meduzareader.interfaces.BaseFragmentEventListener
import com.vedidev.meduzareader.view.fragments.BaseFragment

import butterknife.bindView

abstract class BaseActivity : AppCompatActivity(), BaseFragmentEventListener {
    private val TAG = BaseActivity::class.java.simpleName

    protected val toolbar: Toolbar? by bindView(R.id.toolbar)
    protected val coordinator: CoordinatorLayout? by bindView(R.id.coordinator)

    protected var ourActionBar: ActionBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
        if (toolbar != null) {
            setSupportActionBar(toolbar)
            ourActionBar = supportActionBar
        }
        initView(savedInstanceState)
        setWhiteToolbar()
    }

    fun setToolbarTitle(res: String) {
        supportActionBar?.title = res
    }

    fun setToolbarTitle(@StringRes res: Int) {
        setToolbarTitle(getString(res))
    }

    //this method will correct application toolbar title color on opened apps stack
    //use it if your toolbar text color white, but toolbar is one of light colors

    private fun setWhiteToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val color = ContextCompat.getColor(this, R.color.cardview_dark_background)
            val td = ActivityManager.TaskDescription(null, null, color)
            setTaskDescription(td)
        }
    }

    /* abstract */

    protected abstract val layoutResId: Int

    protected abstract fun initView(savedInstanceState: Bundle?)

    /* Display snacks & toasts */

    protected fun shortToast(@StringRes id: Int) {
        Toast.makeText(applicationContext, id, Toast.LENGTH_SHORT).show()
    }

    protected fun longToast(@StringRes id: Int) {
        Toast.makeText(applicationContext, id, Toast.LENGTH_LONG).show()
    }

    protected fun shortToast(msg: String) {
        Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT).show()
    }

    protected fun longToast(msg: String) {
        Toast.makeText(applicationContext, msg, Toast.LENGTH_LONG).show()
    }

    protected fun shortSnack(@StringRes id: Int) {
        if (coordinator != null) {
            Snackbar.make(coordinator!!, id, Snackbar.LENGTH_SHORT).show()
        }
    }

    protected fun shortSnack(value: String?) {
        if (coordinator != null) {
            Snackbar.make(coordinator!!, value!!, Snackbar.LENGTH_SHORT).show()
        }
    }

    protected fun longSnack(@StringRes id: Int) {
        if (coordinator != null) {
            Snackbar.make(coordinator!!, id, Snackbar.LENGTH_LONG).show()
        }
    }

    protected fun longSnack(value: String?) {
        if (coordinator != null) {
            Snackbar.make(coordinator!!, value!!, Snackbar.LENGTH_LONG).show()
        }
    }

    protected fun actionSnack(@StringRes id: Int, @StringRes but: Int, listener: View.OnClickListener) {
        if (coordinator != null) {
            Snackbar.make(coordinator!!, id, Snackbar.LENGTH_INDEFINITE)
                    .setAction(but, listener)
                    .addCallback(object : Snackbar.Callback() {
                        override fun onDismissed(snackbar: Snackbar?, event: Int) {
                            if (event == Snackbar.Callback.DISMISS_EVENT_SWIPE) {
                                actionSnackRecursive(id, but, listener)
                            }
                        }
                    }).show()
        }
    }

    protected fun actionSnackRecursive(@StringRes id: Int, @StringRes but: Int, listener: View.OnClickListener) {
        if (coordinator != null) {
            Snackbar.make(coordinator!!, id, Snackbar.LENGTH_INDEFINITE)
                    .setAction(but, listener)
                    .addCallback(object : Snackbar.Callback() {
                        override fun onDismissed(snackbar: Snackbar?, event: Int) {
                            if (event == Snackbar.Callback.DISMISS_EVENT_SWIPE) {
                                actionSnackRecursive(id, but, listener)
                            }
                        }
                    }).show()
        }
    }

    // base fragment event listener

    override fun launchActivity(clzz: Class<*>, isFinish: Boolean) {
        startActivity(Intent(this, clzz))
        overridePendingTransition(R.anim.fast_fade_in, R.anim.fast_fade_out)
        if (isFinish) {
            finish()
        }
    }

    override fun launchIntent(intent: Intent) {
        try {
            startActivity(intent)
        } catch (e: Exception) {
            Log.e(TAG, "launchIntent: ", e)
        }

    }

    override fun openFragment(fragment: BaseFragment<*>, addToBackStack: Boolean) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.tagName)
        }
        fragmentTransaction.replace(R.id.fragment_container, fragment, fragment.tagName).commit()
    }
}
