package com.vedidev.meduzareader.view.activities

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import butterknife.bindView

import com.vedidev.meduzareader.App
import com.vedidev.meduzareader.R
import com.vedidev.domain.common.StateMaintainer
import com.vedidev.meduzareader.di.AppComponent
import com.vedidev.meduzareader.interfaces.presenter.BasePresenterActions
import com.vedidev.meduzareader.interfaces.view.BaseViewActions


abstract class BaseViewActivity<T : BasePresenterActions.ViewActions> : BaseActivity(), BaseViewActions {
    internal val progressView: View? by bindView(R.id.progress_view)
    internal val emptyView: View? by bindView(R.id.empty_view)

    protected var progressDialog: ProgressDialog? = null
    protected var presenter: T? = null

    private var stateMaintainer: StateMaintainer? = StateMaintainer.instance
    private var fadeInAnimation: Animation? = null
    private var fadeOutAnimation: Animation? = null
    override var isUiLocked: Boolean = false

    /* Superclass methods */

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fadeOutAnimation = AnimationUtils.loadAnimation(viewContext, R.anim.fast_fade_out)
        fadeInAnimation = AnimationUtils.loadAnimation(viewContext, R.anim.fast_fade_in)
        restore()
    }

    protected fun restore() {
        if (stateMaintainer!!.firstTimeInForActivity(tagName)) {
            initialize()
        } else {
            reinitialize()
        }
    }

    protected fun resetView() {
        presenter?.setView(this)
    }

    override fun onStart() {
        super.onStart()
        presenter?.onStart()
    }

    override fun onResume() {
        super.onResume()
        presenter?.onResume()
    }

    public override fun onPause() {
        super.onPause()
        presenter?.onPause()
    }

    override fun onStop() {
        super.onStop()
        presenter?.onStop()
        try {
            hideKeyboard()
        } catch (e: Exception) {
            //nothing to do
        }
    }

    public override fun onDestroy() {
        stateMaintainer?.delete(tagName)
        presenter?.onDestroy()
        presenter = null
        super.onDestroy()
    }

    override val viewContext: Context
        get() = this

    override fun injector(): AppComponent? {
        return (application as App).appComponent
    }

    override fun showProgressBar() {
        progressDialog?.show()
    }

    override fun showProgressView() {
        progressView?.visibility = View.VISIBLE
        emptyView?.visibility = View.GONE
    }

    override fun hideProgressView() {
        progressView?.visibility = View.GONE
    }

    override fun showEmptyView() {
        emptyView?.visibility = View.VISIBLE
    }

    override fun lockUi() {
        isUiLocked = true
    }

    override fun unlockUi() {
        isUiLocked = false
//        hideProgress()
    }

    override fun hideKeyboard() {
        val imm = viewContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }


    /* Inner methods */

    protected fun fadeAction(view: View, start: Boolean) {
        view.visibility = if (start) View.VISIBLE else View.GONE
        view.startAnimation(if (start) fadeInAnimation else fadeOutAnimation)
    }

    private fun initialize() {
        presenter = createPresenter()
        stateMaintainer?.put(tagName, presenter)
        initProgressDialog()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(this)
        if (progressTitle != null) {
            progressDialog?.setTitle(progressTitle)
        }
        if (progressMessage != null) {
            progressDialog?.setMessage(progressMessage)
        }
    }

    private fun hideProgress() {
        progressDialog?.dismiss()
        progressView?.visibility = View.GONE
    }

    private fun reinitialize() {
        presenter = stateMaintainer?.get<T>(tagName)
        if (presenter == null) {
            initialize()
        } else {
            resetView()
        }
    }

    protected val progressTitle: String?
        get() = null

    protected val progressMessage: String?
        get() = getString(R.string.progress_message)

    /* Abstract methods */

    protected abstract fun createPresenter(): T

    abstract val tagName: String
}
