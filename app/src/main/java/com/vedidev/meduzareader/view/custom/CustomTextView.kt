package com.vedidev.meduzareader.view.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import com.vedidev.domain.utils.FontUtils
import com.vedidev.meduzareader.R

class CustomTextView : TextView {

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        applyCustomFont(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        applyCustomFont(context, attrs)
    }

    constructor(context: Context) : super(context)

    private fun applyCustomFont(context: Context, attrs: AttributeSet) {
        val ta = context.theme.obtainStyledAttributes(attrs, R.styleable.CustomTextView, 0, 0)
        val font = ta.getString(R.styleable.CustomTextView_font)
        selectTypeface(context, font)
    }

    private fun selectTypeface(context: Context, font: String) {
        this.typeface = FontUtils.instance.getTypeface(font, context)
    }
}
