package com.vedidev.meduzareader.view.adapters

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.bindView
import com.vedidev.data.entities.beans.Source
import com.vedidev.meduzareader.R

class SourcesAdapter(var sources: List<Source>) : RecyclerView.Adapter<SourcesAdapter.CategoriesHolder>() {

    private var listener: CardClickListener? = null

    override fun getItemCount(): Int = sources.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CategoriesHolder =
            CategoriesHolder(LayoutInflater.from(parent?.context).inflate(R.layout.card_sources, parent, false))

    override fun onBindViewHolder(holder: CategoriesHolder?, position: Int) {
        holder?.name?.text = sources[position].name
        holder?.description?.text = sources[position].description
        holder?.card?.setOnClickListener {
            if (listener != null) {
                listener?.onCardClick(sources[position].id!!)
            }
        }
    }

    class CategoriesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView by bindView(R.id.source_name)
        val description: TextView by bindView(R.id.source_description)
        val card: CardView by bindView(R.id.card_view)
    }

    fun setCardClickListener(cardClickListener: CardClickListener) {
        this.listener = cardClickListener
    }

    interface CardClickListener {
        fun onCardClick(name: String)
    }
}