package com.vedidev.meduzareader.view.activities

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.bindView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.vedidev.data.entities.beans.Article
import com.vedidev.domain.common.AppError
import com.vedidev.domain.common.Constants
import com.vedidev.meduzareader.R
import com.vedidev.meduzareader.interfaces.presenter.FeedPresenterActions
import com.vedidev.meduzareader.interfaces.view.FeedViewActions
import com.vedidev.meduzareader.presenter.FeedPresenter

class FeedActivity : BaseViewActivity<FeedPresenterActions.ViewActions>(), FeedViewActions {

    val articleTitle: TextView by bindView(R.id.tv_title)
    val progressBar: ProgressBar by bindView(R.id.article_progress_bar)
    val articleImage: ImageView by bindView(R.id.article_image)
    val articleDescription: TextView by bindView(R.id.article_description)
    val articleDate: TextView by bindView(R.id.article_date)

    var bookmarkItem: MenuItem? = null

    var isFound: Boolean = false

    override val layoutResId: Int = R.layout.activity_single_feed
    override fun createPresenter(): FeedPresenterActions.ViewActions = FeedPresenter(this)
    override val tagName: String = FeedActivity::class.java.simpleName
    override fun showError(error: AppError?) = shortSnack(error!!.message)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter?.onViewCreated()
    }

    override fun initView(savedInstanceState: Bundle?) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_single_feed, menu)
        bookmarkItem = menu?.findItem(R.id.item_bookmark)
        isArticleFound(isFound)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        presenter?.onOptionsItemSelected(item)
        return true
    }

    override fun getArticle(): String? {
        return intent.getStringExtra(Constants.KEY_ARTICLE)
    }

    override fun setArticle(article: Article) {
        articleTitle.text = article.title
        articleDescription.text = article.description
        articleDate.text = article.publishedAt
        Glide
                .with(articleImage.context)
                .load(article.urlToImage)
                .listener(object : RequestListener<Drawable> {
                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        progressBar.visibility = View.GONE
                        return false
                    }

                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        progressBar.visibility = View.GONE
                        return false
                    }

                })
                .into(articleImage)
    }

    override fun isArticleFound(isFound: Boolean) {
        this.isFound = isFound
        if (isFound) {
            bookmarkItem?.setIcon(R.drawable.ic_star_black_24dp)
        } else {
            bookmarkItem?.setIcon(R.drawable.ic_star_white_24dp)
        }
    }
}

