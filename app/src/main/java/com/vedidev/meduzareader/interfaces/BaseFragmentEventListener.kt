package com.vedidev.meduzareader.interfaces

import android.content.Intent
import com.vedidev.domain.common.AppError

import com.vedidev.meduzareader.view.fragments.BaseFragment

interface BaseFragmentEventListener {
    fun launchActivity(clzz: Class<*>, isFinish: Boolean)

    fun launchIntent(intent: Intent)

    fun openFragment(fragment: BaseFragment<*>, addToBackStack: Boolean)

    fun showError(error: AppError?)

    //    void showDialogFragment(BaseDialogFragment dialogFragment);
}
