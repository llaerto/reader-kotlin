package com.vedidev.meduzareader.interfaces.model

import com.vedidev.meduzareader.di.AppComponent

interface BaseModelActions {
    fun injector(): AppComponent?
    fun onStart()
    fun onResume()
    fun onPause()
    fun onStop()
    fun onDestroy()
}
