package com.vedidev.meduzareader.interfaces.model

interface FeedsModelActions : BaseModelActions {
    fun getArticles(sourceName: String)
    fun getBookmarks(type: String)

}
