package com.vedidev.meduzareader.interfaces.view

import com.vedidev.meduzareader.view.fragments.CategoriesFragment
import com.vedidev.meduzareader.view.fragments.FeedsFragment

interface DrawerViewActions : BaseViewActions {
    fun showFeedsFragment(feedsFragment: FeedsFragment)
    fun showCategoriesFragment(categoriesFragment: CategoriesFragment)
    fun showBookmarksFragment(feedsFragment: FeedsFragment)
}
