package com.vedidev.meduzareader.interfaces.presenter

import com.vedidev.data.entities.response.container.SourcesResponse

interface CategoriesPresenterActions : BasePresenterActions {

    interface ViewActions : BasePresenterActions.ViewActions {
        fun onCardClick(name: String)
    }

    interface ModelActions : BasePresenterActions.ModelActions {
        fun onSourceLoaded(sourcesResponse: SourcesResponse?)
    }
}
