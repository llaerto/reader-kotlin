package com.vedidev.meduzareader.interfaces.model

import com.vedidev.data.entities.beans.Article

interface FeedModelActions : BaseModelActions {
    fun checkArticle(article: Article)
    fun addItem(article: Article)
    fun removeItem(article: Article)
}
