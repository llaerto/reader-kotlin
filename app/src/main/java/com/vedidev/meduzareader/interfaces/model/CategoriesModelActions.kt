package com.vedidev.meduzareader.interfaces.model

interface CategoriesModelActions : BaseModelActions {
    fun getSources(type: String)
}
