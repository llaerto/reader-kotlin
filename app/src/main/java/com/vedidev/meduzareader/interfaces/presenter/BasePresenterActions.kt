package com.vedidev.meduzareader.interfaces.presenter

import com.vedidev.domain.common.AppError
import com.vedidev.meduzareader.di.AppComponent
import com.vedidev.meduzareader.interfaces.view.BaseViewActions

interface BasePresenterActions {
    /* presenter actions - should communicate model with view through presenter */

    /* View -> Presenter actions
     * actions should communicate from view to presenter. Presenter should do something
     * and decide next step if it is necessary
     */
    interface ViewActions {
        fun setView(view: BaseViewActions?)
        fun onViewCreated()
        fun onStart()
        fun onResume()
        fun onPause()
        fun onStop()
        fun onDestroy()
        fun onViewDestroyed()
    }

    /* Model -> Presenter
     * actions should communicate from model to presenter. Presenter should do something
     * and decide next step if it is necessary
     */
    interface ModelActions {
        fun onError(error: AppError?)
        fun injector(): AppComponent?
    }
}
