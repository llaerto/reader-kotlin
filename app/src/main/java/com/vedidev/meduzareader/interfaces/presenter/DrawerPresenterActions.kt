package com.vedidev.meduzareader.interfaces.presenter

interface DrawerPresenterActions : BasePresenterActions {

    interface ViewActions : BasePresenterActions.ViewActions {
        fun onItemSelect(item: Int)
    }

    interface ModelActions : BasePresenterActions.ModelActions
}
