package com.vedidev.meduzareader.interfaces.view

import android.content.Intent
import com.vedidev.data.entities.beans.Article

interface FeedsViewActions : BaseViewActions {
    fun showFeeds(feeds: List<Article>)
    fun getType(): String?
    fun refreshAdapter()
    fun launchFeedActivity(feedIntent: Intent)
}
