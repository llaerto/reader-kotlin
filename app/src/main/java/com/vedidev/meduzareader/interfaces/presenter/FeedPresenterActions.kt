package com.vedidev.meduzareader.interfaces.presenter

import android.view.MenuItem

interface FeedPresenterActions : BasePresenterActions {

    interface ViewActions : BasePresenterActions.ViewActions {
        fun onBookmarkItemClick(isFound: Boolean)
        fun onOptionsItemSelected(item: MenuItem?)
    }

    interface ModelActions : BasePresenterActions.ModelActions {
        fun isArticleFound(isFound: Boolean)
    }
}
