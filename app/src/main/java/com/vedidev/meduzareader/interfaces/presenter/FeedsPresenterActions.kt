package com.vedidev.meduzareader.interfaces.presenter

import com.vedidev.data.entities.beans.Article
import com.vedidev.data.entities.response.container.ArticlesResponse

interface FeedsPresenterActions : BasePresenterActions {

    interface ViewActions : BasePresenterActions.ViewActions {
        fun onCardClick(article: Article)
    }

    interface ModelActions : BasePresenterActions.ModelActions {
        fun onArticleLoaded(articlesResponse: ArticlesResponse?)
        fun onBookmarksLoaded(articles: ArrayList<Article>)
    }
}
