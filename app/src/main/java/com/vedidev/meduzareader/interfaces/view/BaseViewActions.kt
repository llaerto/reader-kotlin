package com.vedidev.meduzareader.interfaces.view

import android.content.Context

import com.vedidev.domain.common.AppError
import com.vedidev.meduzareader.di.AppComponent

interface BaseViewActions {
    val viewContext: Context?
    fun injector(): AppComponent?
    fun showError(error: AppError?)
    fun showProgressBar()
    fun showProgressView()
    fun showEmptyView()
    fun lockUi()
    val isUiLocked: Boolean
    fun unlockUi()
    fun hideKeyboard()
    fun hideProgressView()
}
