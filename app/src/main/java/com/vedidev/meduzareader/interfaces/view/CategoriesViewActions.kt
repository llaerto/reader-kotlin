package com.vedidev.meduzareader.interfaces.view

import com.vedidev.data.entities.beans.Source
import com.vedidev.meduzareader.view.fragments.FeedsFragment

interface CategoriesViewActions : BaseViewActions {
    fun getType(): String?
    fun showItems(sources: ArrayList<Source>)
    fun refreshAdapter()
    fun showFeedsFragment(feedsFragment: FeedsFragment)
}
