package com.vedidev.meduzareader.interfaces.view

import com.vedidev.data.entities.beans.Article

interface FeedViewActions : BaseViewActions {
    fun getArticle(): String?
    fun setArticle(article: Article)
    fun isArticleFound(isFound: Boolean)
}
