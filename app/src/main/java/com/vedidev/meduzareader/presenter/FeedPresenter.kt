package com.vedidev.meduzareader.presenter

import android.view.MenuItem
import com.google.gson.Gson
import com.vedidev.data.entities.beans.Article
import com.vedidev.meduzareader.R
import com.vedidev.meduzareader.interfaces.model.FeedModelActions
import com.vedidev.meduzareader.interfaces.presenter.FeedPresenterActions
import com.vedidev.meduzareader.interfaces.view.FeedViewActions
import com.vedidev.meduzareader.model.FeedModel

class FeedPresenter(view: FeedViewActions)
    : BasePresenter<FeedViewActions, FeedModelActions>(view),
        FeedPresenterActions.ViewActions, FeedPresenterActions.ModelActions {


    var isFound: Boolean = false
    var article: Article? = null

    override fun createModel(): FeedModelActions? = FeedModel(this)

    override fun onViewCreated() {
        if (getView()?.getArticle() != null) {
            article = Gson().fromJson(getView()?.getArticle(), Article::class.java)
            getView()?.setArticle(article!!)
            model?.checkArticle(article!!)
        }
    }

    override fun onBookmarkItemClick(isFound: Boolean) {

    }

    override fun onOptionsItemSelected(item: MenuItem?) {
        if (item?.itemId == R.id.item_bookmark) {
            if (!isFound && article != null) {
                model?.addItem(article!!)
            } else if (isFound && article != null) {
                model?.removeItem(article!!)
            }
        }
    }

    override fun isArticleFound(isFound: Boolean) {
        this.isFound = isFound
        getView()?.isArticleFound(isFound)
    }
}