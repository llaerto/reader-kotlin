package com.vedidev.meduzareader.presenter

import android.content.Context

import com.vedidev.domain.common.AppError
import com.vedidev.meduzareader.di.AppComponent
import com.vedidev.meduzareader.interfaces.model.BaseModelActions
import com.vedidev.meduzareader.interfaces.presenter.BasePresenterActions
import com.vedidev.meduzareader.interfaces.view.BaseViewActions

import rx.subscriptions.CompositeSubscription
import java.lang.ref.WeakReference

abstract class BasePresenter<T : BaseViewActions, M : BaseModelActions> constructor(view: T) : BasePresenterActions.ViewActions, BasePresenterActions.ModelActions {

    var isStarted: Boolean = false
    var isRunning: Boolean
    var view: WeakReference<T>?
    var model: M?

    protected var compositeSubscription: CompositeSubscription? = CompositeSubscription()

    init {
        isRunning = true
        this.view = WeakReference(view)
        this.model = createModel()
        compositeSubscription = CompositeSubscription()
        subscribeOnEvents()
    }

    @Suppress("UNCHECKED_CAST")
    override fun setView(view: BaseViewActions?) {
        isRunning = true
        this.view = WeakReference(view as T)
        this.model = createModel()
        compositeSubscription = CompositeSubscription()
        subscribeOnEvents()
    }

    override fun injector(): AppComponent? {
        return getView()?.injector()
    }

    /* Inner methods */

    protected abstract fun createModel(): M?

    protected fun getView(): T? {
        return view?.get()
    }

    protected val context: Context?
        get () {
            return getView()?.viewContext
        }

    protected fun subscribeOnEvents() {
        /* here should be subscription to events
         * override if necessary */
    }

    /* View calls */

    override fun onStart() {
        isStarted = true
    }

    override fun onResume() {
        isRunning = true
    }

    override fun onPause() {
        isRunning = false
    }

    override fun onStop() {
        isStarted = false
    }

    override fun onViewDestroyed() {
        view = null
    }

    override fun onDestroy() {
        compositeSubscription?.clear()
        isRunning = false
        isStarted = false
        view?.clear()
        view = null
        model = null
    }

    /* Model callbacks */

    override fun onError(error: AppError?) {
        getView()?.unlockUi()
        getView()?.showError(error)
    }

}
