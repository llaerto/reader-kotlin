package com.vedidev.meduzareader.presenter

import com.google.gson.Gson
import com.vedidev.data.entities.beans.Source
import com.vedidev.data.entities.response.container.SourcesResponse
import com.vedidev.meduzareader.interfaces.model.CategoriesModelActions
import com.vedidev.meduzareader.interfaces.presenter.CategoriesPresenterActions
import com.vedidev.meduzareader.interfaces.view.CategoriesViewActions
import com.vedidev.meduzareader.model.CategoriesModel
import com.vedidev.meduzareader.view.fragments.FeedsFragment
import org.json.JSONArray
import org.json.JSONObject

class CategoriesPresenter(view: CategoriesViewActions)
    : BasePresenter<CategoriesViewActions, CategoriesModelActions>(view),
        CategoriesPresenterActions.ViewActions,
        CategoriesPresenterActions.ModelActions {

    private val sources: ArrayList<Source> = ArrayList()

    override fun createModel(): CategoriesModelActions? = CategoriesModel(this)

    override fun onViewCreated() {
        getView()?.showProgressView()
        if (getView()?.getType() != null) {
            model?.getSources(getView()?.getType()!!)
        }
        getView()?.showItems(sources)
    }

    override fun onSourceLoaded(sourcesResponse: SourcesResponse?) {
        getView()?.hideProgressView()
        val jsonArray: JSONArray = JSONObject(sourcesResponse.toString()).getJSONArray("sources")
        (0..(jsonArray.length() - 1))
                .map { Gson().fromJson(jsonArray[it].toString(), Source::class.java) }
                .forEach {
                    sources.add(it)
                    getView()?.refreshAdapter()
                }
    }

    override fun onCardClick(name: String) {
        getView()?.showFeedsFragment(FeedsFragment.newInstance(name))
    }
}
