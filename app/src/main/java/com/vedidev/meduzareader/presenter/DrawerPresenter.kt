package com.vedidev.meduzareader.presenter

import com.vedidev.meduzareader.R
import com.vedidev.meduzareader.interfaces.model.DrawerModelActions
import com.vedidev.meduzareader.interfaces.presenter.DrawerPresenterActions
import com.vedidev.meduzareader.interfaces.view.DrawerViewActions
import com.vedidev.meduzareader.model.DrawerModel
import com.vedidev.meduzareader.view.fragments.CategoriesFragment
import com.vedidev.meduzareader.view.fragments.FeedsFragment

class DrawerPresenter(view: DrawerViewActions)
    : BasePresenter<DrawerViewActions, DrawerModelActions>(view),
        DrawerPresenterActions.ViewActions,
        DrawerPresenterActions.ModelActions {
    override fun createModel(): DrawerModelActions? = DrawerModel(this)


    override fun onViewCreated() {

    }

    override fun onItemSelect(item: Int) {
        when (item) {
            R.id.drawer_bookmarks -> showFeedsFragment(context?.getString(R.string.drawer_bookmarks)!!)
            R.id.categories_business -> showFragment(context?.getString(R.string.categories_business)!!)
            R.id.categories_entertainment -> showFragment(context?.getString(R.string.categories_entertainment)!!)
            R.id.categories_gaming -> showFragment(context?.getString(R.string.categories_gaming)!!)
            R.id.categories_general -> showFragment(context?.getString(R.string.categories_general)!!)
            R.id.categories_music -> showFragment(context?.getString(R.string.categories_music)!!)
            R.id.categories_politics -> showFragment(context?.getString(R.string.categories_politics)!!)
            R.id.categories_science_and_nature -> showFragment(context?.getString(R.string.categories_science_and_nature_api)!!)
            R.id.categories_sport -> showFragment(context?.getString(R.string.categories_sport)!!)
            R.id.categories_technology -> showFragment(context?.getString(R.string.categories_technology)!!)
        }

    }

    private fun showFeedsFragment(name: String) {
        getView()?.showBookmarksFragment(FeedsFragment.newInstance(name))
    }

    private fun showFragment(source: String) {
        getView()?.showCategoriesFragment(CategoriesFragment.newInstance(source))
    }
}
