package com.vedidev.meduzareader.presenter

import android.content.Intent
import android.util.Log
import com.google.gson.Gson
import com.vedidev.data.entities.beans.Article
import com.vedidev.data.entities.response.container.ArticlesResponse
import com.vedidev.domain.common.Constants
import com.vedidev.meduzareader.interfaces.model.FeedsModelActions
import com.vedidev.meduzareader.interfaces.presenter.FeedsPresenterActions
import com.vedidev.meduzareader.interfaces.view.FeedsViewActions
import com.vedidev.meduzareader.model.FeedsModel
import com.vedidev.meduzareader.view.activities.FeedActivity
import org.json.JSONArray
import org.json.JSONObject

class FeedsPresenter(view: FeedsViewActions)
    : BasePresenter<FeedsViewActions, FeedsModelActions>(view),
        FeedsPresenterActions.ViewActions, FeedsPresenterActions.ModelActions {

    private var articles: ArrayList<Article> = ArrayList()

    override fun createModel(): FeedsModelActions? = FeedsModel(this)

    override fun onViewCreated() {
        getView()?.showProgressView()
        if (getView()?.getType() != null && getView()?.getType() != "Bookmarks") {
            model?.getArticles(getView()?.getType()!!)
        }
    }

    override fun onArticleLoaded(articlesResponse: ArticlesResponse?) {
        getView()?.hideProgressView()
        getView()?.showFeeds(articles)
        val jsonArray: JSONArray = JSONObject(articlesResponse.toString()).getJSONArray("articles")
        (0..(jsonArray.length() - 1))
                .map { Gson().fromJson(jsonArray[it].toString(), Article::class.java) }
                .forEach {
                    articles.add(it)
                    getView()?.refreshAdapter()
                }
    }

    override fun onCardClick(article: Article) {
        val articleString: String = Gson().toJson(article)
        val feedIntent: Intent = Intent(context, FeedActivity::class.java)
        feedIntent.putExtra(Constants.KEY_ARTICLE, articleString)
        getView()?.launchFeedActivity(feedIntent)
    }

    override fun onBookmarksLoaded(articles: ArrayList<Article>) {
        getView()?.hideProgressView()
        getView()?.showFeeds(articles)
    }

    override fun onResume() {
        super.onResume()
        if (getView()?.getType() != null && getView()?.getType() == "Bookmarks") {
            model?.getBookmarks(getView()?.getType()!!)
        }
    }
}
