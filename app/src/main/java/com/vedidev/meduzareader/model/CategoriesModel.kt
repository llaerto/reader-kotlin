package com.vedidev.meduzareader.model

import com.vedidev.data.api.ApiManager
import com.vedidev.data.entities.response.container.SourcesResponse
import com.vedidev.meduzareader.interfaces.model.CategoriesModelActions
import com.vedidev.meduzareader.interfaces.presenter.CategoriesPresenterActions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CategoriesModel(presenter: CategoriesPresenterActions.ModelActions)
    : BaseModel<CategoriesPresenterActions.ModelActions>(presenter),
        CategoriesModelActions {

    @Inject lateinit var manager: ApiManager

    init {
        injector()?.inject(this)
    }

    override fun getSources(type: String) {
        compositeSubscription.add(manager.getSources(type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { throwable ->
                    processError(throwable)
                    null
                }
                .doOnError { this::processError }
                .doOnNext { sourcesResponse -> processSourcesResponse(sourcesResponse) }
                .subscribe())
    }

    private fun processSourcesResponse(sourcesResponse: SourcesResponse?) {
        presenter?.onSourceLoaded(sourcesResponse)
    }
}