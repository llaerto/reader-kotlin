package com.vedidev.meduzareader.model

import android.util.Log
import com.vedidev.data.db.DBManager
import com.vedidev.data.entities.beans.Article
import com.vedidev.meduzareader.interfaces.model.FeedModelActions
import com.vedidev.meduzareader.interfaces.presenter.FeedPresenterActions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FeedModel(presenter: FeedPresenterActions.ModelActions)
    : BaseModel<FeedPresenterActions.ModelActions>(presenter), FeedModelActions {

    @Inject lateinit var manager: DBManager

    init {
        injector()?.inject(this)
    }

    override fun checkArticle(article: Article) {
        compositeSubscription.add(manager.checkArticle(article)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { throwable ->
                    processError(throwable)
                    null
                }
                .doOnError { this::processError }
                .doOnNext { isFound -> isArticleFound(isFound) }
                .subscribe())
    }

    private fun isArticleFound(isFound: Boolean) {
        presenter?.isArticleFound(isFound)
    }

    override fun addItem(article: Article) {
        compositeSubscription.add(manager.putArticle(article)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { throwable ->
                    processError(throwable)
                    null
                }
                .doOnError { this::processError }
                .doOnNext { article -> checkArticle(article) }
                .subscribe())
    }

    override fun removeItem(article: Article) {
        compositeSubscription.add(manager.removeArticle(article)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { throwable ->
                    processError(throwable)
                    null
                }
                .doOnError { this::processError }
                .doOnNext { article -> checkArticle(article) }
                .subscribe())
    }
}
