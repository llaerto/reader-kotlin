package com.vedidev.meduzareader.model

import com.vedidev.domain.common.AppError
import com.vedidev.meduzareader.di.AppComponent
import com.vedidev.meduzareader.interfaces.model.BaseModelActions
import com.vedidev.meduzareader.interfaces.presenter.BasePresenterActions
import io.reactivex.disposables.CompositeDisposable

abstract class BaseModel<T : BasePresenterActions.ModelActions> constructor(presenter: T) : BaseModelActions {

    protected val compositeSubscription: CompositeDisposable = CompositeDisposable()
    protected var presenter: T?

    init {
        this.presenter = presenter
    }

    override fun injector(): AppComponent? {
        return presenter?.injector()
    }
    /* Model operations: presenter -> model */

    override fun onStart() {
        //override if necessary
    }

    override fun onResume() {
        //override if necessary
    }

    override fun onPause() {
        //override if necessary
    }

    override fun onStop() {
        //override if necessary
    }

    override fun onDestroy() {
        compositeSubscription.clear()
        presenter = null
    }

    /* abstract methods */


    /* Inner methods */

    protected fun processError(throwable: Throwable) {
        presenter?.onError(AppError(throwable))
    }
}

