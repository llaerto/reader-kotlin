package com.vedidev.meduzareader.model

import android.util.Log
import com.vedidev.data.api.ApiManager
import com.vedidev.data.db.DBManager
import com.vedidev.data.entities.beans.Article
import com.vedidev.data.entities.response.container.ArticlesResponse
import com.vedidev.meduzareader.interfaces.model.FeedsModelActions
import com.vedidev.meduzareader.interfaces.presenter.FeedsPresenterActions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FeedsModel(presenter: FeedsPresenterActions.ModelActions)
    : BaseModel<FeedsPresenterActions.ModelActions>(presenter),
        FeedsModelActions {


    @Inject lateinit var apiManager: ApiManager
    @Inject lateinit var dbManager: DBManager

    init {
        injector()?.inject(this)
    }

    override fun getArticles(sourceName: String) {
        compositeSubscription.add(apiManager.getArticles(sourceName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { throwable ->
                    processError(throwable)
                    null
                }
                .doOnError { this::processError }
                .doOnNext { articlesResponse -> processArticlesResponse(articlesResponse) }
                .subscribe())
    }

    private fun processArticlesResponse(articlesResponse: ArticlesResponse?) {
        presenter?.onArticleLoaded(articlesResponse)
    }

    override fun getBookmarks(type: String) {
        compositeSubscription.add(dbManager.getArticles()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { throwable ->
                    processError(throwable)
                    null
                }
                .doOnError { this::processError }
                .doOnNext { articles -> processArticlesList(articles) }
                .subscribe())
    }

    private fun processArticlesList(articles: ArrayList<Article>) {
        presenter?.onBookmarksLoaded(articles)
    }
}
