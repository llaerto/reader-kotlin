package com.vedidev.meduzareader.model


import com.vedidev.meduzareader.interfaces.model.DrawerModelActions
import com.vedidev.meduzareader.interfaces.presenter.DrawerPresenterActions

class DrawerModel(presenter: DrawerPresenterActions.ModelActions)
    : BaseModel<DrawerPresenterActions.ModelActions>(presenter),
        DrawerModelActions {
}
