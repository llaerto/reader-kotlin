package com.vedidev.meduzareader.di

import com.vedidev.data.DataModule
import com.vedidev.data.NetModule
import com.vedidev.meduzareader.model.CategoriesModel
import com.vedidev.meduzareader.model.DrawerModel
import com.vedidev.meduzareader.model.FeedModel
import com.vedidev.meduzareader.model.FeedsModel
import com.vedidev.meduzareader.presenter.DrawerPresenter
import com.vedidev.meduzareader.view.activities.DrawerActivity

import javax.inject.Singleton

import dagger.Component


@Singleton
@Component(modules = arrayOf(AppModule::class, NetModule::class, DataModule::class))
interface AppComponent {
    fun inject(activity: DrawerActivity)
    fun inject(categoriesModel: CategoriesModel)
    fun inject(feedsModel: FeedsModel)
    fun inject(feedModel: FeedModel)
}
