package com.vedidev.meduzareader.di


import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.vedidev.meduzareader.App
import com.vedidev.meduzareader.events.RxBus
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(private val app: App) {

    @Provides @Singleton internal fun provideApp(): App {
        return app
    }

    @Provides @Singleton internal fun provideRxBus(): RxBus {
        return RxBus()
    }

    @Provides @Singleton internal fun providesSharedPreferences(): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app)
    }
}
