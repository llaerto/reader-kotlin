package com.vedidev.meduzareader

import android.app.Application
import com.vedidev.data.DataModule
import com.vedidev.meduzareader.di.AppModule
import com.vedidev.domain.common.Constants
import com.vedidev.meduzareader.di.AppComponent
import com.vedidev.meduzareader.di.DaggerAppComponent
import com.vedidev.data.NetModule

class App : Application() {
    var appComponent: AppComponent? = null
        private set

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .netModule(NetModule(Constants.SERVER_URL))
                .dataModule(DataModule(cacheDir.absolutePath))
                .build()
    }
}
