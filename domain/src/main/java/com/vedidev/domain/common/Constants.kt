package com.vedidev.domain.common

object Constants {
    val APP_TAG = "App"
    val ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android"
    //network
    val SERVER_URL = "https://newsapi.org/v1/"
    val API_KEY = "0b588a49bf32447cbe1863a9602e2eec"
    //time constants
    val ONE_SECOND = 1000
    val TEN_SECONDS = ONE_SECOND * 10
    val ONE_MINUTE = 60 * ONE_SECOND
    val ONE_HOUR = 60 * ONE_MINUTE
    //timeouts
    val DEFAULT_SERVER_READ_TIMEOUT = 15L
    val DEFAULT_SERVER_CONNECT_TIMEOUT = 15L
    //another
    val KEY_ARTICLE = "article"

}
