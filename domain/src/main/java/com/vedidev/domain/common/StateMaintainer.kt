package com.vedidev.domain.common

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

import java.util.HashMap

class StateMaintainer private constructor() {

    private val data: HashMap<String, Any?>?

    init {
        this.data = HashMap<String, Any?>()
    }

    fun firstTimeIn(fragmentManager: FragmentManager, fragmentTag: String): Boolean {
        try {
            var stateMaintainerFrag: Fragment? = fragmentManager.findFragmentByTag(fragmentTag)
            if (stateMaintainerFrag == null) {
                stateMaintainerFrag = Fragment()
                fragmentManager.beginTransaction()
                        .add(stateMaintainerFrag, fragmentTag).commit()
                return true
            }
            return false
        } catch (e: NullPointerException) {
            return false
        }

    }

    fun firstTimeInForActivity(activityTag: String): Boolean {
        return data == null || data.size == 0 || data.containsKey(activityTag)
    }

    fun delete(key: String) {
        if (data != null && data.containsKey(key)) {
            data.remove(key)
        }
    }

    fun put(key: String, obj: Any?) {
        data!!.put(key, obj)
    }

    @Suppress("UNCHECKED_CAST")
    operator fun <T> get(key: String): T {
        return data!![key] as T
    }


    private object Holder { val INSTANCE = StateMaintainer() }

    companion object {
        val instance: StateMaintainer by lazy { Holder.INSTANCE }
    }

}
