package com.vedidev.domain.common

class AppError {
    var code: Int? = null
    var throwable: Throwable? = null
    var message: String? = null
        get() {
            if (field == null) {
                if (throwable != null) {
                    this.message = throwable!!.localizedMessage
                }
            }
            return field
        }

    constructor()

    constructor(throwable: Throwable) {
        this.throwable = throwable
    }

    constructor(message: String) {
        this.message = message
    }

}
