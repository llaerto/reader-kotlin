package com.vedidev.domain.utils

class ValidateUtils @javax.inject.Inject
constructor() {

    fun nullToString(nullToString: String?): String {
        return nullToString ?: ""
    }

    fun validMail(email: String?): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(nullToString(email)).matches()
    }

    fun tvtoString(view: android.widget.TextView?): String {
        return view?.text.toString()
    }
}
