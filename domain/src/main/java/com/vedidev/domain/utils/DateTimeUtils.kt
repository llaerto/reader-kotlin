package com.vedidev.domain.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

object DateTimeUtils {
    val ISO_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
    val SDF_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"

    private val SDF_DATE_TIME = SimpleDateFormat(SDF_TIME_FORMAT, Locale.getDefault())

    val currentTimeString: String
        get() {
            val today = Calendar.getInstance()
            return formatAndroidTime(today)
        }

    fun format(date: Calendar): String {
        return SDF_DATE_TIME.format(date.time)
    }

    fun parse(s: String): Calendar? {
        try {
            val date = SDF_DATE_TIME.parse(s)
            val calendar = Calendar.getInstance()
            calendar.time = date
            return calendar
        } catch (e: ParseException) {
            return null
        }

    }

    private fun formatAndroidTime(date: Calendar): String {
        val timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        timeFormat.calendar = date
        return timeFormat.format(date.time)
    }
}
