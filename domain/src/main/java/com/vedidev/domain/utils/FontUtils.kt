package com.vedidev.domain.utils

import android.graphics.Typeface
import java.util.Hashtable

public class FontUtils private constructor() {
    private val fontCache = Hashtable<String, Typeface>()

    private object Holder { val INSTANCE = FontUtils() }

    companion object {
        val instance: FontUtils by lazy { Holder.INSTANCE }
    }

    fun getTypeface(name: String, context: android.content.Context): Typeface? {
        var tf: Typeface? = fontCache[name]
        if (tf == null) {
            try {
                tf = Typeface.createFromAsset(context.assets, name)
            } catch (e: Exception) {
                return null
            }

            fontCache.put(name, tf)
        }
        return tf
    }
}
